package com.example.saudeBemEstar.repositories;

import com.example.saudeBemEstar.models.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicacaoRepository extends JpaRepository<Medicacao, Long> {}
