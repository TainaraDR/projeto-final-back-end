package com.example.saudeBemEstar.repositories;

import com.example.saudeBemEstar.models.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacienteRepository extends JpaRepository<Paciente, Long> {
}

