package com.example.saudeBemEstar.services;

import com.example.saudeBemEstar.models.Atendimento;
import com.example.saudeBemEstar.repositories.AtendimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AtendimentoService {

    @Autowired
    private AtendimentoRepository repository;

    public List<Atendimento> findAll(int page, int size) {
        return repository.findAll();
    }

    public Atendimento findById(Long id) {
        Optional<Atendimento> atendimentoOptional = repository.findById(id);
        return atendimentoOptional.orElse(null);
    }

    public Atendimento save(Atendimento atendimento) {
        return repository.save(atendimento);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
