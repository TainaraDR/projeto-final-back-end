package com.example.saudeBemEstar.services;

import com.example.saudeBemEstar.models.*;
import com.example.saudeBemEstar.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;


@Service
public class MedicoService {

    @Autowired
    private MedicoRepository medicoRepository;

    public List<Medico> findAll(int page, int size) {
        return medicoRepository.findAll();
    }

    public Medico findById(Long id) {
        Optional<Medico> medicoOptional = medicoRepository.findById(id);
        return medicoOptional.orElse(null);
    }

    public Medico save(Medico medico) {
        return medicoRepository.save(medico);
    }

    public void deleteById(Long id) {
        medicoRepository.deleteById(id);
    }

}

