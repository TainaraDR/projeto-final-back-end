package com.example.saudeBemEstar.services;

import com.example.saudeBemEstar.models.Paciente;
import com.example.saudeBemEstar.repositories.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PacienteService {

    @Autowired
    private PacienteRepository pacienteRepository;

    public List<Paciente> findAll(int page, int size) {
        return pacienteRepository.findAll(PageRequest.of(page, size)).getContent();
    }

    public Paciente findById(Long id) {
        return pacienteRepository.findById(id).orElseThrow(() -> new RuntimeException("Paciente não encontrado"));
    }

    public Paciente save(Paciente paciente) {
        return pacienteRepository.save(paciente);
    }

    public Paciente update(Long id, Paciente updatedPaciente) {
        return pacienteRepository.findById(id)
                .map(paciente -> {
                    paciente.setNomePaciente(updatedPaciente.getNomePaciente());
                    paciente.setCpfPaciente(updatedPaciente.getCpfPaciente());
                    paciente.setDataNascimentoPaciente(updatedPaciente.getDataNascimentoPaciente());
                    return pacienteRepository.save(paciente);
                })
                .orElse(null);
    }

    public void delete(Long id) {
        pacienteRepository.deleteById(id);
    }
}
