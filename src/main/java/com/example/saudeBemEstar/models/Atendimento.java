package com.example.saudeBemEstar.models;

import jakarta.persistence.*;

import java.util.Date;

@Entity
public class Atendimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAtendimento;
    @ManyToOne
    private Paciente paciente;
    @ManyToOne
    private Medico medico;
    private Date dataAtendimento;
    private String descricaoAtendimento;
    @ManyToOne
    private Medicacao medicacao;

    public Atendimento() {
    }

    public Atendimento(Long idAtendimento, Paciente paciente, Medico medico, Date dataAtendimento, String descricaoAtendimento, Medicacao medicacao) {
        this.idAtendimento = idAtendimento;
        this.paciente = paciente;
        this.medico = medico;
        this.dataAtendimento = dataAtendimento;
        this.descricaoAtendimento = descricaoAtendimento;
        this.medicacao = medicacao;
    }

    public Long getIdAtendimento() {
        return idAtendimento;
    }

    public void setIdAtendimento(Long idAtendimento) {
        this.idAtendimento = idAtendimento;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Date getDataAtendimento() {
        return dataAtendimento;
    }

    public void setDataAtendimento(Date dataAtendimento) {
        this.dataAtendimento = dataAtendimento;
    }

    public String getDescricaoAtendimento() {
        return descricaoAtendimento;
    }

    public void setDescricaoAtendimento(String descricaoAtendimento) {
        this.descricaoAtendimento = descricaoAtendimento;
    }

    public Medicacao getMedicacao() {
        return medicacao;
    }

    public void setMedicacao(Medicacao medicacao) {
        this.medicacao = medicacao;
    }
}


