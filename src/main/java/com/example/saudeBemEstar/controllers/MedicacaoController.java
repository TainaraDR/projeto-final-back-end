package com.example.saudeBemEstar.controllers;

import com.example.saudeBemEstar.models.Medicacao;
import com.example.saudeBemEstar.services.MedicacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicacoes")
public class MedicacaoController {

    // Injeção de dependência do serviço
    @Autowired
    private MedicacaoService service;

    @GetMapping
    public ResponseEntity<List<Medicacao>> getAllMedicacoes(@RequestParam(defaultValue = "0") int page,
                                                            @RequestParam(defaultValue = "10") int size) {
        List<Medicacao> medicacoes = service.findAll(page, size);
        return ResponseEntity.ok(medicacoes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Medicacao> getMedicacaoById(@PathVariable Long id) {
        Medicacao medicacao = service.findById(id);
        return medicacao != null ? ResponseEntity.ok(medicacao) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Medicacao> createMedicacao(@RequestBody Medicacao medicacao) {
        Medicacao createdMedicacao = service.save(medicacao);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdMedicacao);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Medicacao> updateMedicacao(@PathVariable Long id, @RequestBody Medicacao medicacaoDetails) {
        Medicacao existingMedicacao = service.findById(id);
        if (existingMedicacao == null) {
            return ResponseEntity.notFound().build();
        }

        existingMedicacao.setNomeMedicacao(medicacaoDetails.getNomeMedicacao());
        existingMedicacao.setFabricante(medicacaoDetails.getFabricante());
        existingMedicacao.setDosagem(medicacaoDetails.getDosagem());
        existingMedicacao.setValidadeMedicacao(medicacaoDetails.getValidadeMedicacao());

        Medicacao updatedMedicacao = service.save(existingMedicacao);
        return ResponseEntity.ok(updatedMedicacao);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMedicacao(@PathVariable Long id) {
        Medicacao existingMedicacao = service.findById(id);
        if (existingMedicacao == null) {
            return ResponseEntity.notFound().build();
        }

        service.deleteById(id);
        return ResponseEntity.ok("Medicação excluída com sucesso");
    }
}
