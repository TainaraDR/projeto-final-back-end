package com.example.saudeBemEstar.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ajuda")
public class AjudaController {

    @GetMapping
    public String getAjuda() {
        return "{\n" +
                "  \"estudantes\": [\"Monique Pavan\", \"Renan Henrique\", \"Tainara da Rosa\"],\n" +
                "  \"projeto\": \"API Bem Viver\",\n" +
                "  \"tema\": \"Saúde e Bem-Estar\"\n" +
                "}";
    }
}
