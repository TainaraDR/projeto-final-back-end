package com.example.saudeBemEstar.controllers;

import com.example.saudeBemEstar.models.Atendimento;
import com.example.saudeBemEstar.services.AtendimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/atendimentos")
public class AtendimentoController {

    // Injeção de dependência do serviço
    @Autowired
    private AtendimentoService atendimentoService;

    @GetMapping
    public ResponseEntity<List<Atendimento>> getAllAtendimentos(@RequestParam(defaultValue = "0") int page,
                                                                @RequestParam(defaultValue = "10") int size) {
        List<Atendimento> atendimentos = atendimentoService.findAll(page, size);
        return ResponseEntity.ok(atendimentos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Atendimento> getAtendimentoById(@PathVariable Long id) {
        Atendimento atendimento = atendimentoService.findById(id);
        return atendimento != null ? ResponseEntity.ok(atendimento) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Atendimento> createAtendimento(@RequestBody Atendimento atendimento) {
        Atendimento createdAtendimento = atendimentoService.save(atendimento);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAtendimento);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Atendimento> updateAtendimento(@PathVariable Long id, @RequestBody Atendimento atendimentoDetails) {
        Atendimento existingAtendimento = atendimentoService.findById(id);
        if (existingAtendimento == null) {
            return ResponseEntity.notFound().build();
        }

        existingAtendimento.setDataAtendimento(atendimentoDetails.getDataAtendimento());
        existingAtendimento.setDescricaoAtendimento(atendimentoDetails.getDescricaoAtendimento());
        existingAtendimento.setMedicacao(atendimentoDetails.getMedicacao());

        Atendimento updatedAtendimento = atendimentoService.save(existingAtendimento);
        return ResponseEntity.ok(updatedAtendimento);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAtendimento(@PathVariable Long id) {
        Atendimento existingAtendimento = atendimentoService.findById(id);
        if (existingAtendimento == null) {
            return ResponseEntity.notFound().build();
        }

        atendimentoService.deleteById(id);
        return ResponseEntity.ok("Atendimento excluído com sucesso");
    }
}
